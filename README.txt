QUICKIE MOD TESTER
==================

This project provides a set of python scripts intended to perform some basic checks on the
Minecraft quickiefabric mod (https://gitlab.com/jottyfan/quickiefabric) by jottyfan.

It still can be used to test the old forge-based quickie mod (https://gitlab.com/jottyfan/quickie).


PREREQUISITES

1. Installed software
   - Python 3.7 or newer
   - PIL (Python Imaging Library) or pillow

2. git clone https://gitlab.com/jottyfan/quickiefabric.git
   into a local folder of your choice

3. git clone https://gitlab.com/p-martin/quickie_mod_tester.git
   into a different local folder of your choice


HOW TO USE IT?

1. cd to the quickie_mod_tester folder

2. run ./quickie_mod_tester --path /path/to/quickiefabric --verbose

3. after the script finished, open /tmp/quickie_test_report.html in your browser
