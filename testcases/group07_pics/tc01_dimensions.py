#!/usr/bin/env python3

import os

from PIL import Image

from context.testcontext import TestContext
from report.testresult import check_true


IGNORE_FILE_EXTENSIONS = ['.xcf']


def run(report):
    report.set_test_result_group('Picture Checks')

    pics_ok = True
    num_files = 0
    msgs = []
    textures_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'resources', 'assets',
                                TestContext.get_mod_name(), 'textures')
    for dirname, __, filelist in os.walk(textures_dir):
        for filename in filelist:
            num_files += 1
            extension = os.path.splitext(filename)[1]
            if extension != '.png':
                if extension in IGNORE_FILE_EXTENSIONS:
                    msgs.append(f'{filename} ignored')
                else:
                    pics_ok = False
                    msgs.append(f'{filename} is not a PNG according to its '
                                f'extension "{extension}"')
            else:
                pics_ok = check_img(os.path.join(dirname, filename), msgs) and pics_ok
    if TestContext.mod_name == 'quickiefabric':
        pics_ok = check_img(os.path.normpath(os.path.join(textures_dir, '..', 'icon.png')),
                            msgs) and pics_ok
    prefix = 'ok, ' if pics_ok else ''
    msgs.append(f'{prefix}{num_files} files checked')

    report.add_test_result('All texture images are 16x16px PNG',
                           '\n'.join(msgs), check_true(pics_ok))


def check_img(img_path, msgs) -> bool:
    try:
        img = Image.open(img_path)
        if img.size != (16, 16):
            msgs.append(f'{os.path.basename(img_path)} is {img.size} pixels')
            return False
    except OSError as exc:
        msgs.append(f'{img_path}: {exc}')
        return False
    return True
