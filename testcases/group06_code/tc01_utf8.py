#!/usr/bin/env python3

import os

from context.testcontext import TestContext
from report.testresult import check_true


IGNORE_EXTENSIONS = ['.jar', '.class', '.png', '.xcf', '.log']


def run(report):
    report.set_test_result_group('Code Analysis')

    encoding_ok = True
    num_files = 0
    msg = 'ok'
    strip_chars = len(TestContext.get_path().rstrip(os.sep)) + 1
    extensions = set()
    for dirname, __, filelist in os.walk(TestContext.get_path()):
        if any([dirname[strip_chars:].startswith(excl)
                for excl in ['.', 'build/', 'run/']]):
            continue
        for filename in filelist:
            num_files += 1
            ext = os.path.splitext(filename)[1]
            if ext in IGNORE_EXTENSIONS:
                continue
            extensions.add(ext)
            filepath = os.path.join(dirname, filename)
            try:
                with open(filepath, encoding='utf-8') as fdesc:
                    fdesc.read()
            except UnicodeDecodeError as exc:
                encoding_ok = False
                msg = f'{filepath[strip_chars:]}: {exc}'
                break
        if not encoding_ok:
            break
    msg = (f'{msg}, {num_files} files checked\n'
           f'extensions {sorted(list(extensions))!r}')

    report.add_test_result('All files have UTF-8 encoding', msg, check_true(encoding_ok))
