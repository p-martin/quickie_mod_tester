#!/usr/bin/env python3

import os

from context.testcontext import TestContext
from misc.filehandling import FileInformation
from misc.helperfunctions import check_valid_item
from report.testresult import check_true


def run(report):
    report.set_test_result_group('Recipes')

    recipes_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'resources', 'data',
                               TestContext.get_mod_name(), 'recipes')
    recipes_filenames = [filename
                         for filename in FileInformation.get_file_list(recipes_dir)
                         if filename.endswith('.json')]
    recipes_ok = True
    msgs = []
    crafting_graph = {}
    num_files = 0
    for filename in recipes_filenames:
        num_files += 1
        try:
            content = FileInformation(os.path.join(recipes_dir, filename)).get_content_json()
            vtype = content['type']
            if vtype.startswith('minecraft:'):
                vtype = vtype.split(':', 1)[1]
            if vtype in ('blasting', 'campfire_cooking', 'crafting_shapeless', 'smelting',
                         'smoking', 'stonecutting'):
                ingredients = content.get('ingredients', content.get('ingredient', []))
                if not ingredients:
                    raise ValueError('no ingredients provided')
                if not isinstance(ingredients, list):
                    ingredients = [ingredients]
                ing_list = []
                for ing in ingredients:
                    ing_list.append(ing['item'])
                    check_valid_item(ing['item'])
                if vtype == 'crafting_shapeless':
                    crafting_graph.setdefault(content['result']['item'], []).append(ing_list)
                    check_valid_item(content['result']['item'])
                else:
                    crafting_graph.setdefault(content['result'], []).append(ing_list)
                    check_valid_item(content['result'])
                    if vtype == 'stonecutting':
                        if 'count' not in content:
                            raise ValueError('"count" missing')
                    else:
                        if 'experience' not in content:
                            raise ValueError('"experience" missing')
            elif vtype in ('crafting_shaped',):
                for character in ''.join(content['pattern']):
                    if not character.isspace():
                        if character not in content['key']:
                            raise ValueError(f'Character "{character}" from pattern not defined '
                                             f'in key items.')
                ingredients = []
                for item_obj in content['key'].values():
                    ingredients.append(item_obj['item'])
                    check_valid_item(item_obj['item'])
                crafting_graph.setdefault(content['result']['item'], []).append(ingredients)
                check_valid_item(content['result']['item'])
            else:
                raise ValueError(f"Unknown type {content['type']!r}.")
        except (ValueError, KeyError) as exc:
            recipes_ok = False
            msgs.append(f'Error in {filename}: {exc}')
    prefix = 'ok, ' if recipes_ok else ''
    msgs.append(f'{prefix}{num_files} files checked')

    mod_name_title = TestContext.get_mod_name().title()
    report.add_test_result(f'Recipes only use Minecraft or {mod_name_title} items',
                           '\n'.join(msgs), check_true(recipes_ok))

    for result, ing_lists in list(crafting_graph.items()):
        if result.startswith('minecraft:'):
            del crafting_graph[result]
            continue
        for i, ing_list in enumerate(ing_lists):
            ing_lists[i] = [ing for ing in ing_list if not ing.startswith('minecraft:')]
    for blockdrop in ('sulphor', 'salpeter'):  # TODO: parse from Java code
        item_name = ':'.join([TestContext.get_mod_name(), blockdrop])
        crafting_graph.setdefault(item_name, []).append([])
    changed = True
    while changed:
        changed = False
        for result, ing_lists in list(crafting_graph.items()):
            if [] in ing_lists:
                del crafting_graph[result]
                for result2, ing_lists2 in crafting_graph.items():
                    for i, ing_list2 in enumerate(ing_lists2):
                        ing_lists2[i] = [ing for ing in ing_list2 if ing != result]
                changed = True
    report.add_test_result('Recipes can be crafted',
                           '\n'.join(f'{k!r} cannot be crafted - needs {v!r}'
                                     for k, v in sorted(crafting_graph.items())) or 'ok',
                           check_true(not crafting_graph))
