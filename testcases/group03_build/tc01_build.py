#!/usr/bin/env python3

from context.testcontext import TestContext
from misc.processhandling import ExternalProgram
from report.testresult import check_true, TestResult


def run(report):
    report.set_test_result_group('Build Process')

    if TestContext.get_fast():
        test_value = ''
        test_result = TestResult.SKIPPED
    else:
        gradle = ExternalProgram(['./gradlew', 'build', '--exclude-task', 'test'])
        test_value = f'returncode={gradle.get_returncode()}'
        test_result = check_true(gradle.get_returncode() == 0)

    report.add_test_result('The mod can be built', test_value, test_result)
