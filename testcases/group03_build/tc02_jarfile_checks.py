#!/usr/bin/env python3

from context.testcontext import TestContext
from misc.filehandling import FileInformation
from report.testresult import check_true, TestResult


def run(report):
    filepath = f'build/libs/{TestContext.get_mod_name()}-{TestContext.get_latest_version()}.jar'
    jar_file = FileInformation(filepath)

    if TestContext.get_fast():
        test_value = ''
        test_result = TestResult.SKIPPED
    else:
        test_value = repr(jar_file.get_existing_filename())
        test_result = check_true(jar_file.exists())

    report.add_test_result(f"JAR file '{filepath}' exists", test_value, test_result)

    if TestContext.get_fast():
        test_value = ''
        test_result = TestResult.SKIPPED
    else:
        mtime = jar_file.get_modification_time()
        if mtime and mtime >= TestContext.get_start_time():
            mtime_ok = True
            test_value = 'ok'
        else:
            mtime_ok = False
            test_value = f'MTime: {mtime.strftime("%d.%m.%Y %H:%M:%S")}' if mtime else 'n.ok'
        test_result = check_true(mtime_ok, result_if_false=TestResult.UNSPECIFIC)

    report.add_test_result('JAR file modified after start of test', test_value, test_result)
