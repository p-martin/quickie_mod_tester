#!/usr/bin/env python3

from context.testcontext import TestContext
from report.testresult import check_true, TestResult


def run(report):
    report.set_test_result_group('Git Checks')

    report.add_test_result('Current commit has a git tag',
                           repr(TestContext.get_git_tag()),
                           check_true(bool(TestContext.get_git_tag()),
                                      result_if_false=TestResult.UNSPECIFIC))
