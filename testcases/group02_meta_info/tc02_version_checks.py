#!/usr/bin/env python3

from context.testcontext import TestContext
from misc.filehandling import FileInformation
from report.testresult import check_true, TestResult


def run(report):
    if TestContext.get_latest_git_tag():
        check_version = '.'.join(TestContext.get_latest_git_tag().split('.')[:3])
    else:
        check_version = '0.0.0'
    if TestContext.get_mod_name() == 'quickie':
        run_q(report, check_version)
    else:
        run_qf(report, check_version)


def run_qf(report, check_version):
    lines = FileInformation('gradle.properties').get_content().decode('utf-8').splitlines()
    for line in lines:
        if line.strip().startswith('mod_version'):
            mod_version = line.split('=')[-1].strip()
            report.add_test_result('mod_version equals git tag',
                                   repr(mod_version),
                                   check_true(mod_version == TestContext.get_latest_git_tag()))
            TestContext.set_latest_version(mod_version)
            break
    else:
        report.add_test_result('mod_version equals git tag',
                               'No line starting with "mod_version =" found in gradle.properties!',
                               TestResult.NOT_OK)
    for line in lines:
        if line.strip().startswith('maven_group'):
            maven_group = line.split('=')[-1].strip()
            report.add_test_result('maven_group',
                                   repr(maven_group),
                                   check_true(maven_group.startswith('de.jottyfan')))
            break
    else:
        report.add_test_result('maven_group',
                               'No line starting with "maven_group =" found in gradle.properties!',
                               TestResult.NOT_OK)
    for line in lines:
        if line.strip().startswith('archives_base_name'):
            abn = line.split('=')[-1].strip()
            report.add_test_result('archives_base_name',
                                   repr(abn),
                                   check_true(abn == TestContext.get_mod_name()))
            break
    else:
        report.add_test_result('archives_base_name',
                               'No line starting with "archives_base_name =" found in '
                               'gradle.properties!',
                               TestResult.NOT_OK)
    mc_version = fab_version = None
    for line in map(str.strip, lines):
        if line.startswith('minecraft_version'):
            mc_version = line.split('=')[-1].strip()
            TestContext.set_minecraft_version(mc_version)
        elif line.startswith('fabric_version'):
            fab_version = line.split('=')[-1].strip()
    if None in (mc_version, fab_version):
        report.add_test_result('fabric_verison matches minecraft_version',
                               'minecraft_version and/or fabric_version not found in '
                               'gradle.properties',
                               TestResult.NOT_OK)
    else:
        report.add_test_result('fabric_version matches minecraft_version',
                               f'minecraft_version = {mc_version!r}, '
                               f'fabric_version = {fab_version!r}',
                               check_true(fab_version.endswith('+' + mc_version)))


def run_q(report, check_version):
    update_json = FileInformation('update.json').get_content_json()

    promo_name = f'{check_version}-latest'
    latest_promo = update_json['promos'].get(promo_name, None)
    report.add_test_result(f'{promo_name} equals latest git tag', repr(latest_promo),
                           check_true(latest_promo == TestContext.get_latest_git_tag()))

    latest_version = max(update_json[check_version].keys())
    report.add_test_result(f'Max({check_version}.x) version equals {promo_name}',
                           repr(latest_version),
                           check_true(latest_promo == latest_version))

    TestContext.set_latest_version(latest_version)
