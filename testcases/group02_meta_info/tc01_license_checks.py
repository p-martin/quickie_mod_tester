#!/usr/bin/env python3

from hashlib import md5

from context.testcontext import TestContext
from misc.filehandling import FileInformation
from report.testresult import check_true


def run(report):
    report.set_test_result_group('Meta Information')

    license_file = FileInformation(['LICENSE', 'LICENSE.txt', 'LICENSE.TXT'])

    report.add_test_result('License file exists',
                           repr(license_file.get_existing_filename()),
                           check_true(license_file.exists()))

    report.add_test_result('License file is big enough',
                           f'{license_file.get_size()} bytes',
                           check_true(license_file.get_size() >= 1024))

    if TestContext.get_mod_name() == 'quickiefabric':
        par_hashes = [md5(par.replace(b'\n', b'').replace(b' ', b'')).hexdigest()
                      for par in license_file.get_content().split(b'\n\n')]
        mit_lic = all(h in par_hashes for h in ['0fb95aee8f0c8f1297039cf39a75daab',
                                                'c4b007f68b32c38ab0f3a0300362a996',
                                                '2b2f22aa257e41d4660d98355c7d40cb'])
        report.add_test_result('License', 'MIT' if mit_lic else 'unknown', check_true(mit_lic))
    else:
        match_list = license_file.get_content_matches(b'GNU LESSER GENERAL PUBLIC LICENSE')
        report.add_test_result('License is LGPL',
                               '',
                               check_true(bool(match_list)))
