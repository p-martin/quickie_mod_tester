#!/usr/bin/env python3

import os

from context.testcontext import TestContext
from misc.filehandling import FileInformation
from report.testresult import check_true, TestResult


def run(report):
    if not TestContext.get_mod_name() == 'quickiefabric':
        return

    resources_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'resources')
    content = FileInformation(os.path.join(resources_dir, 'fabric.mod.json')).get_content_json()
    report.add_test_result('fabric.mod.json: id and name',
                           f'id={content["id"]!r}, name={content["name"]!r}',
                           check_true(content['id'] == TestContext.get_mod_name() and
                                      content['name'] == TestContext.get_mod_name()))

    if 'icon' not in content:
        report.add_test_result('fabric.mod.json: icon', 'no icon defined', TestResult.NOT_OK)
    else:
        icon_path = os.path.join(resources_dir, content['icon'])
        icon_exists = os.path.isfile(icon_path)
        report.add_test_result('fabric.mod.json: icon',
                               '' if icon_exists else f"{content['icon']} doesn't exist",
                               check_true(icon_exists))

    java_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'java')
    msgs = []
    ep_ok = True
    if not content.get('entrypoints', {}):
        ep_ok = False
        msgs.append('no entrypoints defined')
    for ep_name, eps in content['entrypoints'].items():
        if ep_name not in ('main', 'client', 'server'):
            ep_ok = False
            msgs.append(f'unknown entrypoint {ep_name!r}')
        for ep in eps:
            subpath = ep.split('.')
            subpath[-1] += '.java'
            filepath = os.path.join(java_dir, *subpath)
            if not os.path.isfile(filepath):
                ep_ok = False
                msgs.append(f"{filepath} doesn't exist")
    report.add_test_result('fabric.mod.json entrypoints', '\n'.join(msgs), check_true(ep_ok))

    mc_version = mc_version_orig = content['depends']['minecraft']
    if mc_version.startswith('>='):
        mc_version = mc_version[2:].strip()
    report.add_test_result('Minecraft version consistency',
                           f'gradle.properties: {TestContext.get_minecraft_version()!r}, '
                           f'fabric.mod.json: {mc_version_orig!r}',
                           check_true(mc_version == TestContext.get_minecraft_version()))
