#!/usr/bin/env python3

import os

from context.testcontext import TestContext
from misc.filehandling import FileInformation
from report.testresult import check_true


def run(report):
    report.set_test_result_group('Items')

    item_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'resources', 'assets',
                            TestContext.get_mod_name(), 'models', 'item')
    item_filenames = [fn
                      for fn in FileInformation.get_file_list(item_dir)
                      if fn.endswith('.json')]
    items_ok = True
    msgs = []
    num_files = 0
    for filename in item_filenames:
        num_files += 1
        try:
            content = FileInformation(os.path.join(item_dir, filename)).get_content_json()
            if 'textures' in content:
                mod_prefix = TestContext.get_mod_name() + ':'
                for k, v in content['textures'].items():  # pylint: disable=invalid-name
                    if v.startswith('minecraft:'):
                        pass
                    elif v.startswith(mod_prefix):
                        img_path = os.path.join(TestContext.get_path(), 'src', 'main', 'resources',
                                                'assets', TestContext.get_mod_name(), 'textures',
                                                v.split(':', 1)[1]) + '.png'
                        if not os.path.isfile(img_path):
                            raise ValueError(f'Texture image "{v}" does not exist.')
                    else:
                        raise ValueError(f'Unknown texture image reference "{v}".')
                    if k not in ('layer0', 'layer1'):
                        raise ValueError(f'Unknown texture key "{k}".')
            elif 'display' in content:
                pass
            else:
                raise ValueError('Neither "textures" nor "display" found.')
        except ValueError as exc:
            items_ok = False
            msgs.append(f'Error in {filename}: {exc}')
    prefix = 'ok, ' if items_ok else ''
    msgs.append(f'{prefix}{num_files} files checked')

    report.add_test_result('All item models are valid', '\n'.join(msgs), check_true(items_ok))
