#!/usr/bin/env python3

from misc.filehandling import FileInformation
from report.testresult import check_true


def run(report):
    report.set_test_result_group('Documentation')

    readme_md_file = FileInformation('README.md')

    report.add_test_result('README.md file exists',
                           repr(readme_md_file.get_existing_filename()),
                           check_true(readme_md_file.exists()))

    report.add_test_result('README.md file is big enough',
                           f'{readme_md_file.get_size()} bytes',
                           check_true(readme_md_file.get_size() >= 1024))
