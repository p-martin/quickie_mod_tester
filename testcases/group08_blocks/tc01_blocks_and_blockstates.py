#!/usr/bin/env python3

import os
import re

from context.testcontext import TestContext
from misc.filehandling import FileInformation
from report.testresult import check_true


def run(report):
    report.set_test_result_group('Blocks')

    # collect and check all blocks
    block_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'resources', 'assets',
                             TestContext.get_mod_name(), 'models', 'block')
    block_filenames = [fn
                       for fn in FileInformation.get_file_list(block_dir)
                       if fn.endswith('.json')]
    blocks_ok = True
    msg = 'ok'
    num_files = 0
    for filename in block_filenames:
        num_files += 1
        try:
            content = FileInformation(os.path.join(block_dir, filename)).get_content_json()
            mod_prefix = TestContext.get_mod_name() + ':'
            for k, v in content['textures'].items():  # pylint: disable=invalid-name
                if v.startswith('minecraft:'):
                    pass
                elif v.startswith(mod_prefix):
                    img_path = os.path.join(TestContext.get_path(), 'src', 'main', 'resources',
                                            'assets', TestContext.get_mod_name(), 'textures',
                                            v.split(':', 1)[1]) + '.png'
                    if not os.path.isfile(img_path):
                        raise ValueError(f'Texture image "{v}" does not exist.')
                else:
                    raise ValueError(f'Unknown texture image reference "{v}".')
                if k not in ('all', 'end', 'side', 'cross', 'up', 'down', 'north', 'south',
                             'east', 'west', 'particle', 'bottom'):
                    raise ValueError(f'Unknown texture position "{k}".')
        except (ValueError, KeyError) as exc:
            blocks_ok = False
            msg = f'Error in {filename}: {exc}'
            break
    msg = f'{msg}, {num_files} files checked'
    report.add_test_result('All block models are valid', msg, check_true(blocks_ok))

    blockstate_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'resources', 'assets',
                                  TestContext.get_mod_name(), 'blockstates')
    blockstate_filenames = [fn
                            for fn in FileInformation.get_file_list(blockstate_dir)
                            if fn.endswith('.json')]
    referenced_block_filenames = set()
    blockstates_ok = True
    msg = 'ok'
    num_files = 0
    for filename in blockstate_filenames:
        num_files += 1
        try:
            content = FileInformation(os.path.join(blockstate_dir, filename)).get_content_json()
            mod_prefix = TestContext.get_mod_name() + ':block/'
            for k, var in content['variants'].items():
                if k != '' and not re.match(r'^age=[0-7]$', k):
                    raise ValueError(f'unknown variant "{k}"')
                v = var['model']
                if v.startswith(mod_prefix):
                    block_model_filename = v.split('/')[1] + '.json'
                    block_model_path = os.path.join(block_dir, block_model_filename)
                    if not FileInformation(block_model_path).exists():
                        raise ValueError(f'referenced block model {block_model_path} does not '
                                         f'exist.')
                    referenced_block_filenames.add(block_model_filename)
                else:
                    raise ValueError(f'"{v}" is not a {TestContext.get_mod_name()} block '
                                     f'reference.')
        except (ValueError, KeyError) as exc:
            blockstates_ok = False
            msg = f'Error in {filename}: {exc}'
            break
    msg = f'{msg}, {num_files} files checked'
    report.add_test_result('All blockstate files are valid', msg, check_true(blockstates_ok))

    diff = set(block_filenames).difference(referenced_block_filenames)
    report.add_test_result('Unused block models',
                           '\n'.join(diff),
                           check_true(not diff))
