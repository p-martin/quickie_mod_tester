#!/usr/bin/env python3

import os

from context.testcontext import TestContext
from misc.filehandling import FileInformation
from report.testresult import check_true, TestResult


def run(report):
    report.set_test_result_group('Translation')

    consistent = True
    msg = 'ok'
    translations_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'resources', 'assets',
                                    TestContext.get_mod_name(), 'lang')
    translations = sorted([filename
                           for filename in FileInformation.get_file_list(translations_dir)
                           if filename.endswith('.json')])

    report.add_test_result('At least 2 translation files', repr(len(translations)),
                           check_true(len(translations) >= 2))

    master_key_set = set()
    if translations:
        master, others = translations[0], translations[1:]
        if others:
            master_json = FileInformation(os.path.join(translations_dir,
                                                       master)).get_content_json()
            master_key_set = set(master_json.keys())
            for other in others:
                other_json = FileInformation(os.path.join(translations_dir,
                                                          other)).get_content_json()
                other_key_set = set(other_json.keys())
                if master_key_set.difference(other_key_set):
                    consistent = False
                    msg = (f'Messages from {master} missing in {other}: ' +
                           ', '.join(list(master_key_set.difference(other_key_set))))
                    break
                elif other_key_set.difference(master_key_set):
                    consistent = False
                    msg = (f'Messages from {other} missing in {master}: ' +
                           ', '.join(list(other_key_set.difference(master_key_set))))
                    break
                for key, master_text in master_json.items():
                    other_text = other_json[key]
                    if master_text and not other_text:
                        consistent = False
                        msg = f'Message {key} is empty in {other}, but not in {master}'
                        break
                    elif other_text and not master_text:
                        consistent = False
                        msg = f'Message {key} is empty in {master}, but not in {other}'
                        break
                    master_placeholders = master_text.replace('%%', '').count('%')
                    other_placeholders = other_text.replace('%%', '').count('%')
                    if master_placeholders != other_placeholders:
                        consistent = False
                        msg = (f'Message {key} has {master_placeholders} placeholders in {master}, '
                               f'but {other_placeholders} placeholders in {other}')
                        break
                if not consistent:
                    break
    else:
        consistent = False
        msg = 'No translation files found in '+translations_dir

    report.add_test_result('Consistent translation files', msg, check_true(consistent))

    java_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'java')
    pattern = b'\\bTranslatableText\\s*\\(\\s*"(.*?)"'
    used_translations = set()
    for dirname, __, filelist in os.walk(java_dir):
        for filename in filelist:
            if filename.endswith('.java'):
                filepath = os.path.join(dirname, filename)
                for match in FileInformation(filepath).get_content_matches(pattern):
                    used_translations.add(match.group(1).decode('utf-8'))
    missing_translations = used_translations.difference(master_key_set)
    report.add_test_result('Missing translations',
                           '\n'.join(missing_translations),
                           check_true(not missing_translations))
    unused_translations = master_key_set.difference(used_translations)
    report.add_test_result('Unused translations',
                           '\n'.join(unused_translations),
                           check_true(not unused_translations,
                                      result_if_false=TestResult.UNSPECIFIC))
