#!/usr/bin/env python3

"""
Provides a class which is able to generate a test report as an HTML file.
"""

from time import perf_counter
from html import escape

from context.testcontext import TestContext
from misc.logfunctions import log
from report.testresult import TestResult
from misc.helperfunctions import micros_to_timestr


class TestReport:
    """
    Represents the report which will be filled with information during this test suite.
    At any time, it can be exported as an HTML file.
    Missing information will cause a placeholder to be created.
    """
    def __init__(self, title: str):
        object.__init__(self)
        self.title = title
        self.test_result_groups = {}
        self.current_group_name = ''
        self.last_timestamp = perf_counter()

    def set_test_result_group(self, group_name: str):
        """Sets the current group name for test cases grouping."""
        self.current_group_name = group_name

    def add_test_result(self, test_case_name: str, result: str,
                        test_result: TestResult = TestResult.NO_RESULT):
        """Adds a test result to the current group."""
        if self.current_group_name not in self.test_result_groups:
            self.test_result_groups[self.current_group_name] = []
        new_timestamp = perf_counter()
        delta_t_us = round((new_timestamp - self.last_timestamp) * 1000000)
        self.last_timestamp = new_timestamp
        self.test_result_groups[self.current_group_name].append((test_case_name,
                                                                 result,
                                                                 test_result,
                                                                 delta_t_us))
        log(1, '{0} ... {1}', test_case_name, test_result.name)

    def print_html(self):
        """Prints the collected test results and additional information as HTML to stdout."""
        with open(TestContext.get_outfile(), 'w', encoding='utf-8') as trf:
            trf.write('<!DOCTYPE html><html><head>')
            trf.write('<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />')
            trf.write(f'<title>{self.title}</title>')
            trf.write('''<style type="text/css">
body, td {
    font: 12pt 'Segoe UI 8','Segoe UI',sans-serif;
}
body {
    padding-bottom: 90vh;
}
th {
    font-weight: bold;
    color: #7f7f7f;
    background-color: #dfdfff;
}
h2, h3 {
    margin-top: 1.5em;
}
table {
    border-collapse: collapse;
}
td, th {
    text-align: left;
    vertical-align: top;
    border: #bfbfbf 1px solid;
    padding: 0.5ex;
}
td.h2 {
    border-left: none;
    border-right: none;
    padding-left: 0;
}
thead.sticky th {
    position: sticky;
    top: 0;
}
.right {
    text-align: right;
}
.center {
    text-align: center;
}
.test_result_SKIPPED {
    background-color: #bfbfbf;
}
.test_result_NO_RESULT {
}
.test_result_OK {
    background-color: #7fff7f;
}
.test_result_UNSPECIFIC {
    background-color: #ffff7f;
}
.test_result_NOT_OK {
    background-color: #ff7f7f;
}
.test_result_ERROR {
    background-color: #7f0000;
    color: #ffffff;
}
pre {
    font-size: 8pt;
}
</style>''')
            trf.write('</head><body>')
            trf.write(f'<h1>{self.title}</h1>')
            trf.write(f'<table>')
            start_time = TestContext.get_start_time()
            if start_time:
                start_time_str = start_time.strftime('%Y-%m-%d %H:%M:%S')
                trf.write(f'<tr><td>Start Time</td><td>{start_time_str}</td></tr>')
            mod_name = TestContext.get_mod_name()
            git_commit_hash = TestContext.get_git_commit_hash()
            git_commit_url = f'https://gitlab.com/jottyfan/{mod_name}/-/commit/{git_commit_hash}'
            trf.write(f'<tr><td>Git Commit Hash:</td><td><a href="{git_commit_url}" '
                      f'target="_blank" title="View Commit on GitLab">'
                      f'{git_commit_hash}</a></td></tr>')
            latest_git_tag = TestContext.get_latest_git_tag()
            latest_git_tag_url = f'https://gitlab.com/jottyfan/{mod_name}/-/tags/{latest_git_tag}'
            latest_git_tag_commit_hash = TestContext.get_latest_git_tag_commit_hash()
            latest_git_tag_commit_url = (f'https://gitlab.com/jottyfan/{mod_name}/-/commit/'
                                         f'{latest_git_tag_commit_hash}')
            if latest_git_tag:
                trf.write(f'<tr><td>Latest Git Tag:</td><td><a href="{latest_git_tag_url}" '
                          f'target="_blank" title="View Tag on GitLab">{escape(latest_git_tag)}'
                          f'</a>')
                if latest_git_tag_commit_hash != git_commit_hash:
                    trf.write(f' (different hash: <a href="{latest_git_tag_commit_url}" '
                              f'target="_blank" title="View Commit on GitLab">'
                              f'{latest_git_tag_commit_hash}</a>)')
                trf.write('</td></tr>')
            trf.write(f'</table>')
            pos = 1
            for group_idx, (group_name, test_group) in enumerate(self.test_result_groups.items()):
                if group_idx == 0:
                    trf.write(f'<h2>{escape(group_name)}</h2>')
                else:
                    trf.write(f'<tr><td colspan="5" class="h2"><h2>{escape(group_name)}</h2></td>')
                if test_group:
                    if group_idx == 0:
                        trf.write('''
                                <table><thead class="sticky">
                                <tr>
                                <th class="right">Pos</th>
                                <th>Test Case Name</th>
                                <th>Test Result</th>
                                <th class="center">Evaluation</th>
                                <th>Time</th>
                                </tr>
                                </thead><tbody>
                                ''')
                    for test_case_name, result, test_result, delta_t_us in test_group:
                        if result is not None and result.startswith('<pre>'):
                            detailinfos = result
                        else:
                            detailinfos = '<br />'.join(escape(result).splitlines())
                        trf.write(f'''
                                  <tr>
                                  <td class="right">{pos}.</td>
                                  <td>{escape(test_case_name)}</td>
                                  <td>{detailinfos}</td>
                                  <td class="center test_result_{test_result.name}">
                                      {test_result.name}
                                  </td>
                                  <td class="right">{micros_to_timestr(delta_t_us)}</td>
                                  </tr>
                                  ''')
                        pos += 1
                    if group_idx == len(self.test_result_groups) - 1:
                        trf.write('</tbody></table>')
            trf.write('</body></html>')
            log(1, 'HTML test report written to {0}', TestContext.get_outfile())
