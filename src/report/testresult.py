#!/usr/bin/env python3

"""
Provides a class which represents test result values as an enumerated type.
"""

import enum


class TestResult(enum.IntEnum):
    """All possible test result values."""
    SKIPPED = -1
    NO_RESULT = 0
    OK = 1  # pylint: disable=invalid-name
    UNSPECIFIC = 2
    NOT_OK = 3
    ERROR = 4


def check_true(condition: bool, result_if_false: TestResult = TestResult.NOT_OK) -> TestResult:
    """
    Checks whether the given condition is True/False/None, and returns a
    TestResult.OK/FAILED/NONE respectively.
    """
    if condition is True:
        return TestResult.OK
    elif condition is False:
        if not isinstance(result_if_false, TestResult):
            raise ValueError(f'check_true: parameter result_if_false must be a TestResult, '
                             f'but a {type(result_if_false).__qualname__} was given.')
        return result_if_false
    return TestResult.NO_RESULT
