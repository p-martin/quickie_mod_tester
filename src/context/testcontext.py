#!/usr/bin/env python3

"""
Provides a class which holds information about the context the tester is running in.
"""

from datetime import datetime
from typing import Optional


class TestContext:
    """Stores context information about the test."""
    verbosity: int = 0
    start_time: Optional[datetime] = None
    path: Optional[str] = None
    outfile: Optional[str] = None
    fast: bool = False
    git_commit_hash: Optional[str] = None
    git_tag: Optional[str] = None
    latest_git_tag: Optional[str] = None
    latest_git_tag_commit_hash: Optional[str] = None
    latest_version: Optional[str] = None
    mod_name: Optional[str] = None
    minecraft_version: Optional[str] = None

    @classmethod
    def set_verbosity(cls, verbosity: int):
        """Sets the script's verbosity."""
        cls.verbosity = verbosity

    @classmethod
    def set_start_time(cls, start_time: datetime):
        """Sets the time when the overall test started."""
        cls.start_time = start_time

    @classmethod
    def get_start_time(cls) -> Optional[datetime]:
        """Returns the time when the overall test started."""
        return cls.start_time

    @classmethod
    def set_path(cls, path: str):
        """Sets the path to the mod's base directory."""
        cls.path = path

    @classmethod
    def get_path(cls) -> Optional[str]:
        """Returns the path to the mod's base directory."""
        return cls.path

    @classmethod
    def set_outfile(cls, outfile: str):
        """Sets the filename of the test report."""
        cls.outfile = outfile

    @classmethod
    def get_outfile(cls) -> Optional[str]:
        """Returns the filename of the test report."""
        return cls.outfile

    @classmethod
    def set_fast(cls, fast: bool):
        """Sets the fast flag (skip time-consuming test steps)."""
        cls.fast = fast

    @classmethod
    def get_fast(cls) -> bool:
        """Gets the fast flag (skip time-consuming test steps)."""
        return cls.fast

    @classmethod
    def set_git_commit_hash(cls, git_commit_hash: str):
        """Sets the Git latest Git commit hash."""
        cls.git_commit_hash = git_commit_hash

    @classmethod
    def get_git_commit_hash(cls) -> Optional[str]:
        """Returns the latest Git commit hash."""
        return cls.git_commit_hash

    @classmethod
    def set_git_tag(cls, git_tag: str):
        """Sets the Git tag of the latest Git commit hash."""
        cls.git_tag = git_tag

    @classmethod
    def get_git_tag(cls) -> Optional[str]:
        """Returns the Git tag of the latest Git commit hash."""
        return cls.git_tag

    @classmethod
    def set_latest_git_tag(cls, git_tag: str):
        """Sets the latest Git tag."""
        cls.latest_git_tag = git_tag

    @classmethod
    def get_latest_git_tag(cls) -> Optional[str]:
        """Returns the latest Git tag."""
        return cls.latest_git_tag

    @classmethod
    def set_latest_git_tag_commit_hash(cls, git_commit_hash: str):
        """Sets the Git commit hash that belongs to the latest Git tag."""
        cls.latest_git_tag_commit_hash = git_commit_hash

    @classmethod
    def get_latest_git_tag_commit_hash(cls) -> Optional[str]:
        """Returns the Git commit hash that belongs to the latest Git tag."""
        return cls.latest_git_tag_commit_hash

    @classmethod
    def set_latest_version(cls, latest_version: str):
        """Sets the latest version of the mod."""
        cls.latest_version = latest_version

    @classmethod
    def get_latest_version(cls) -> Optional[str]:
        """Returns the latest version of the mod."""
        return cls.latest_version

    @classmethod
    def set_mod_name(cls, mod_name: str):
        """Sets the mod's name - "quickie" or "quickiefabric"."""
        cls.mod_name = mod_name

    @classmethod
    def get_mod_name(cls) -> Optional[str]:
        """Returns the mod's name."""
        return cls.mod_name

    @classmethod
    def set_minecraft_version(cls, minecraft_version: str):
        """Sets the Minecraft version for which the mod is suitable"""
        cls.minecraft_version = minecraft_version

    @classmethod
    def get_minecraft_version(cls) -> Optional[str]:
        """Returns the Minecraft version"""
        return cls.minecraft_version
