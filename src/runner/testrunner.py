#!/usr/bin/env python3

"""
Provides the core function that runs all the tests.
"""

import importlib
import os
import traceback
from datetime import datetime

from context.testcontext import TestContext
from misc.processhandling import ExternalProgram
from report.testreport import TestReport
from report.testresult import TestResult


def run_tests():
    """Runs the tests using the options from the TestContext."""

    TestContext.set_start_time(datetime.now())

    lines = ExternalProgram(['git', 'log', '-42', '--format=%H/%D']).get_stdout_str().splitlines()
    for i, line in enumerate(lines):
        tmp_hash, tmp_tag = [s.strip() for s in line.split('/', 1)]
        tmp_tag = tmp_tag[4:].lstrip() if tmp_tag.startswith('tag:') else ''
        if i == 0:
            # determine the latest commit hash, and its tag (if any)
            TestContext.set_git_commit_hash(tmp_hash)
            if tmp_tag:
                TestContext.set_git_tag(tmp_tag)
        if tmp_tag:
            # determine the latest git tag (if any), and its commit hash
            TestContext.set_latest_git_tag(tmp_tag)
            TestContext.set_latest_git_tag_commit_hash(tmp_hash)
            break

    qf_check_dir = os.path.join(TestContext.get_path(), 'src', 'main', 'java', 'de', 'jottyfan',
                                'minecraft', 'quickiefabric')
    mod_name = 'quickiefabric' if os.path.isdir(qf_check_dir) else 'quickie'
    TestContext.set_mod_name(mod_name)

    report = TestReport(f'Test Report - {mod_name.title()} Mod')

    testcasesdir = os.path.join(os.path.abspath(__file__).rsplit('/src/', 1)[0], 'testcases')
    for groupname in sorted(os.listdir(testcasesdir)):
        if groupname.startswith('group'):
            groupdir = os.path.join(testcasesdir, groupname)
            for tcname in sorted(os.listdir(groupdir)):
                if tcname.startswith('tc') and tcname.endswith('.py'):
                    module_to_import = f'testcases.{groupname}.{tcname[:-3]}'
                    try:
                        importlib.import_module(module_to_import).run(report)
                    except Exception as exc:
                        report.add_test_result(module_to_import,
                                               '\n'.join(traceback.format_exception(
                                                   type(exc), exc, exc.__traceback__)),
                                               TestResult.ERROR)

    report.print_html()
