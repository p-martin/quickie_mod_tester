#!/usr/bin/env python3

"""
Provides functions which can simplify things inside other modules.
"""
import os.path

from context.testcontext import TestContext


def micros_to_timestr(micros: int) -> str:
    """Converts micro seconds to a human-readable unit."""
    if micros >= 1000000:
        seconds = round(micros * 0.0001) * 0.01
        return '%.2f s' % seconds
    if micros >= 1000:
        millis = round(micros * 0.01) * 0.1
        return '%.1f ms' % millis
    return f'{micros} µs'


def check_valid_prefix(block_or_item_name: str) -> None:
    """
    Checks whether the given block or item name has a valid prefix. Currently, valid prefixes are
    'minecraft:' and 'quickie[fabric]:'. Raises a ValueError if not.
    """
    if not isinstance(block_or_item_name, str):
        raise ValueError(f'Block or item name is not a str, '
                         f'but a {type(block_or_item_name).__qualname__} with the '
                         f'value {repr(block_or_item_name)}')
    if (not block_or_item_name.startswith('minecraft:') and
            not block_or_item_name.startswith(TestContext.get_mod_name() + ':')):
        raise ValueError(f'Block or item name "{block_or_item_name}" has invalid prefix.')


def check_valid_item(item_name: str) -> None:
    """
    Checks whether the given item name is valid. Raises a ValueError if not.
    """
    check_valid_prefix(item_name)
    if item_name.split(':')[0] == TestContext.get_mod_name():
        item_filename = item_name.split(':')[-1] + '.json'
        item_path = os.path.join(TestContext.get_path(), 'src', 'main', 'resources', 'assets',
                                 TestContext.get_mod_name(), 'models', 'item', item_filename)
        if not os.path.isfile(item_path):
            raise ValueError(f'No item file named {item_filename!r} was found.')
