#!/usr/bin/env python3

"""
Provides functions to send text to the standard output.
Also takes care of the log level and the configured verbosity.
"""

from context.testcontext import TestContext


def log(level: int, formatstring: str, *args, **kwargs):
    """
    Prints the given string (and parameters) to stdout, if the level is less or equal than the
    configured verbosity (-v command line options).
    """
    if level <= TestContext.verbosity:
        print(formatstring.format(*args, **kwargs))


def log_no_newline(level: int, formatstring: str, *args, **kwargs):
    """
    Prints the given string (and parameters) to stdout, if the level is less or equal than the
    configured verbosity (-v command line options).
    Does not add an implicit line feed.
    """
    if level <= TestContext.verbosity:
        print(formatstring.format(*args, **kwargs), end='')
