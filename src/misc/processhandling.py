#!/usr/bin/env python3

"""
Provides a class to handle an external process, pass command line arguments, and retrieve its
stdout/stderr and exit code.
"""

import re
import subprocess
from typing import Union, List, Optional

from context.testcontext import TestContext


class ExternalProgram:
    """
    Spawns an external program using command line arguments.
    The exit code as well as stdout and/or stderr may then be examined in order to retrieve
    relevant information, and in order to check it against expected values.
    """
    @staticmethod
    def _get_regex_matches(regex: Union[bytes, re.Pattern], in_what: bytes) -> List[re.Match]:
        if isinstance(regex, str):
            raise TypeError('Please provide the regular expression as bytes (b"...") or '
                            'as re.Pattern, not as str ("...").')
        if isinstance(regex, bytes):
            regex = re.compile(regex)
        return list(regex.finditer(in_what))

    def __init__(self, cmdline_args: List[str], cwd: Optional[str] = None):
        object.__init__(self)
        self.popen = subprocess.Popen(cmdline_args, stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                                      cwd=(TestContext.get_path() if cwd is None else cwd))
        (self.stdout, self.stderr) = self.popen.communicate()

    def get_returncode(self) -> int:
        """Returns the process's return code."""
        return self.popen.returncode

    def assert_returncode_equals(self, expected: int):
        """
        Checks whether the return code meets the given expectation and
        throws an AssertionError if not.
        """
        if self.get_returncode() == expected:
            return
        raise AssertionError(f'Return code {expected} expected, '
                             f'but it was {self.get_returncode()}.')

    def assert_returncode_not_equals(self, expected: int):
        """
        Checks whether the return code meets the given expectation and
        throws an AssertionError if not.
        """
        if self.get_returncode() != expected:
            return
        raise AssertionError(f'Return code {expected} expected, '
                             f'but it was {self.get_returncode()}.')

    def get_stdout_bytes(self) -> bytes:
        """Returns the stdout of the process as bytes."""
        return self.stdout

    def get_stdout_str(self, encoding: str = 'utf-8') -> str:
        """Returns the stdout of the process as str."""
        return self.stdout.decode(encoding)

    def get_stdout_regex_matches(self, regex: Union[bytes, re.Pattern]) -> List[re.Match]:
        """Find regular expression matches in the stdout of the process."""
        return self._get_regex_matches(regex, self.stdout)

    def get_stderr_bytes(self) -> bytes:
        """Returns the stderr of the process as bytes."""
        return self.stderr

    def get_stderr_str(self, encoding: str = 'utf-8') -> str:
        """Returns the stderr of the process as str."""
        return self.stderr.decode(encoding)

    def get_stderr_regex_matches(self, regex: Union[bytes, re.Pattern]) -> List[re.Match]:
        """Find regular expression matches in the stderr of the process."""
        return self._get_regex_matches(regex, self.stderr)
