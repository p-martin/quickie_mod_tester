#!/usr/bin/env python3

"""
Provides a class which serves as a simple interface to a file.
"""

import json
import os
import re
from datetime import datetime
from typing import Union, List, Optional

from context.testcontext import TestContext


class FileInformation:
    """
    References a file on the disk - existing or not - and determines information about it.
    """
    def __init__(self, names: Union[str, List[str]]):
        object.__init__(self)
        self.names = [names] if isinstance(names, str) else names

    @staticmethod
    def get_file_list(path) -> List[str]:
        """Returns a list of filenames inside the given path."""
        relative_path = os.path.join(TestContext.get_path(), path)
        return [filename
                for filename in os.listdir(relative_path)
                if not os.path.isdir(os.path.join(relative_path, filename))]

    def get_existing_filename(self) -> Optional[str]:
        """Return the name of the first existing file in the list of possible names."""
        for filename in self.names:
            if os.path.exists(os.path.join(TestContext.get_path(), filename)):
                return filename
        return None

    def exists(self) -> bool:
        """Returns whether a possible file name matches the name of an existing file."""
        return bool(self.get_existing_filename())

    def get_existing_filepath(self) -> Optional[str]:
        """Return the path of the file, relative to the execution folder of this script."""
        filename = self.get_existing_filename()
        if filename:
            return os.path.join(TestContext.get_path(), filename)
        return None

    def get_size(self) -> int:
        """Returns the size of the first existing file."""
        filepath = self.get_existing_filepath()
        if not filepath:
            return 0
        return os.path.getsize(filepath)

    def get_content(self) -> bytes:
        """Returns the content of the file, read as binary."""
        with open(self.get_existing_filepath(), 'rb') as file_desc:
            return file_desc.read()

    def get_content_json(self):
        """Returns the content of the file, interpreted as JSON."""
        return json.loads(self.get_content())

    def get_content_matches(self, regex: Union[bytes, re.Pattern]) -> List[re.Match]:
        """Find regular expression matches in the first existing file."""
        if isinstance(regex, str):
            raise TypeError('Please provide the regular expression as bytes (b"...") or '
                            'as re.Pattern, not as str ("...").')
        if isinstance(regex, bytes):
            regex = re.compile(regex)
        return list(regex.finditer(self.get_content()))

    def get_modification_time(self) -> Optional[datetime]:
        """Returns the mtime of the first existing file."""
        filepath = self.get_existing_filepath()
        if not filepath:
            return None
        return datetime.fromtimestamp(os.path.getmtime(filepath))
